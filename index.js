'use strict';

var app = angular.module('testapp', []);

// here some work is to be done..
app.component('myList', {
  controller: ['$scope', 'data', function($scope, data) {
    $scope.test = 'Hi Quang';
  }],
  templateUrl: 'list.tpl.html'
});



// mocked data service - don't touch
app.service('data', [function() {
  console.log('data service ready');
  return {
    getAll: function() {
      return Promise.resolve(['foo', 'bar', 'more']);
    },
    select: function(entity) {
      console.log('selecting', entity);
      return Promise.resolve(true);
    }
  };
}]);
