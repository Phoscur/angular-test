'use strict';

var TEMPLATE_FILES = '*.html';
var JS_FILES = '*.js';

module.exports = function(grunt) {
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-contrib-jshint');
  grunt.initConfig({
    watch: {
      text: {
        files: TEMPLATE_FILES,
        options: {
          livereload:true
        }
      },
      js: {
        files: JS_FILES,
        tasks: ['jshint'],
        options: {
          livereload:true
        }
      }
    },
    jshint: {
      files: JS_FILES,
      options: {
        jshintrc: '.jshintrc'
      }
    }
  });
  grunt.registerTask('default', ['watch']);
};
